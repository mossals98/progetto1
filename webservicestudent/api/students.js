import express from "express";
import db from "../db/database";
import Product from "../domain/student";

const router = express.Router();

router.get("/", (req, res, next) => {

    db.query(Product.getAllStudentSQL(), (err, data)=> {
        if(!err) {
            res.status(200).json({
                message:"Students listed.",
                productId:data
            });
        }
    });
});

router.post("/add", (req, res, next) => {
    let product = new Student(req.body.std_name, req.body.std_surname);

    db.query(product.getAddStudentSQL(), (err, data)=> {
        res.status(200).json({
            message:"Student added.",
            studentId: data.insertId
        });
    });
});

router.get("/:studenttId", (req, res, next) => {
    let pid = req.params.studentId;

    db.query(Product.getStudentByIdSQL(pid), (err, data)=> {
        if(!err) {
            if(data && data.length > 0) {

                res.status(200).json({
                    message:"Student found.",
                    product: data
                });
            } else {
                res.status(200).json({
                    message:"Student Not found."
                });
            }
        }
    });
});
 );

module.exports = router;
