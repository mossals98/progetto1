--
--  MySQL
--
DROP TABLE IF EXISTS  students;
CREATE TABLE students (
    std_id     INT(10) NOT NULL AUTO_INCREMENT,
    std_name  VARCHAR(150),
    std_surname  VARCHAR(150),
    PRIMARY KEY (std_id)
);



INSERT INTO students (std_name, std_surname) VALUES ('Studente1', 'mossali');

COMMIT;
